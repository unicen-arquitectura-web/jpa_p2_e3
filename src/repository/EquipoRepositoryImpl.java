package repository;

import java.util.List;
import entity.Equipo;

public class EquipoRepositoryImpl implements EquipoRepository {

	public static EquipoRepositoryImpl instance = new EquipoRepositoryImpl();

	private EquipoRepositoryImpl() {
	}

	public static EquipoRepositoryImpl get_instance() {
		return instance;
	}

	@Override
	public Equipo findById(Integer id) {

		return RepositoryFactory.getEntity_manager().find(Equipo.class, id);
	}

	@Override
	public List<Equipo> findAll() {
		return RepositoryFactory.getEntity_manager().createQuery("SELECT p FROM Persona p", Equipo.class)
				.getResultList();
	}

	@Override
	public Equipo save(Equipo equipo) {
		RepositoryFactory.getEntity_manager().getTransaction().begin();
		if (equipo.getId() == null) {
			RepositoryFactory.getEntity_manager().persist(equipo);
			RepositoryFactory.getEntity_manager().getTransaction().commit();
			RepositoryFactory.cerrar_conexion();
			return equipo;
		}
		RepositoryFactory.getEntity_manager().merge(equipo);
		RepositoryFactory.getEntity_manager().getTransaction().commit();
		RepositoryFactory.cerrar_conexion();
		return equipo;
	}

	@Override
	public void delete(Equipo equipo) {
		RepositoryFactory.getEntity_manager().remove(equipo);
	}
}