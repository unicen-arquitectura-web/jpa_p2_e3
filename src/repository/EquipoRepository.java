package repository;

import entity.Equipo;

public interface EquipoRepository extends BaseRepository<Equipo, Integer> {
    // Metodos especificos de PersonaRepository
}
