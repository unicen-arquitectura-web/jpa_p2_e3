package repository;

import java.util.List;
import entity.Jugador;

public class JugadorRepositoryImpl implements JugadorRepository {

	public static JugadorRepositoryImpl instance = new JugadorRepositoryImpl();

	private JugadorRepositoryImpl() {
	}

	public static JugadorRepositoryImpl get_instance() {
		return instance;
	}

	@Override
	public Jugador findById(Integer id) {
		return RepositoryFactory.getEntity_manager().find(Jugador.class, id);
	}

	@Override
	public List<Jugador> findAll() {
		return RepositoryFactory.getEntity_manager().createQuery("SELECT p FROM Persona p", Jugador.class)
				.getResultList();
	}

	@Override
	public Jugador save(Jugador jugador) {
		RepositoryFactory.getEntity_manager().getTransaction().begin();
		if (jugador.getId() == null) {
			RepositoryFactory.getEntity_manager().persist(jugador);
			RepositoryFactory.getEntity_manager().getTransaction().commit();
			RepositoryFactory.cerrar_conexion();
			return jugador;
		}
		jugador = RepositoryFactory.getEntity_manager().merge(jugador);
		RepositoryFactory.getEntity_manager().getTransaction().commit();
		RepositoryFactory.cerrar_conexion();
		return jugador;

	}

	@Override
	public void delete(Jugador jugador) {
		RepositoryFactory.getEntity_manager().remove(jugador);
	}
}