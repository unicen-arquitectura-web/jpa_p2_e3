package repository;

import entity.Jugador;

public interface JugadorRepository extends BaseRepository<Jugador, Integer> {
    // Metodos especificos de PersonaRepository
}
