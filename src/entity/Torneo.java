package entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Torneo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nombreTorneo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "equipo_torneo")
	private List<Equipo> equipos_participantes;

	public Torneo() {
		super();
	}

	public Torneo(String nombreTorneo, List<Equipo> equipos_participantes) {
		super();
		this.nombreTorneo = nombreTorneo;
		this.equipos_participantes = equipos_participantes;
	}

	public String getNombreTorneo() {
		return nombreTorneo;
	}

	public void setNombreTorneo(String nombreTorneo) {
		this.nombreTorneo = nombreTorneo;
	}

	public List<Equipo> getEquiposParticipantes() {
		return equipos_participantes;
	}

	public void setEquiposParticipantes(List<Equipo> equipos_participantes) {
		this.equipos_participantes = equipos_participantes;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Torneo [id=" + id + ", nombreTorneo=" + nombreTorneo + ", equiposParticipantes=" + equipos_participantes
				+ "]";
	}

}