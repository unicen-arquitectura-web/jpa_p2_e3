package entity;

import javax.persistence.*;

@Entity
public class Equipo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nombre;

	private String tecnico;

	private String representacion;

	@OneToMany(mappedBy = "equipo")
	private EquipoJugador equipo_jugador;
	
	public Equipo(String nombre, String tecnico, String representacion) {
		super();
		this.nombre = nombre;
		this.tecnico = tecnico;
		this.representacion = representacion;
	}

	public Equipo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTecnico() {
		return tecnico;
	}

	public void setTecnico(String tecnico) {
		this.tecnico = tecnico;
	}

	public String getRepresentacion() {
		return representacion;
	}

	public void setRepresentacion(String representacion) {
		this.representacion = representacion;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Equipo [id=" + id + ", nombre=" + nombre + ", tecnico=" + tecnico
				+ ", representacion=" + representacion + "]";
	}

}
