package entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class EquipoJugador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "equipo_id")
	private Equipo equipo;

	@ManyToOne
	@JoinColumn(name = "jugador_id")
	private Jugador jugador;
	
	private boolean es_titular;

	public EquipoJugador(Equipo equipo, Jugador jugador, boolean es_titular) {
		this.equipo = equipo;
		this.jugador = jugador;
		this.es_titular = es_titular;
	}

	public EquipoJugador() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public boolean isEsTitular() {
		return es_titular;
	}

	public void setEsTitular(boolean es_titular) {
		this.es_titular = es_titular;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "EquipoJugador [id=" + id + ", equipo=" + equipo + ", jugador=" + jugador + ", es_titular=" + es_titular
				+ "]";
	}

}
